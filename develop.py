import os
import sys
from app import app

os.environ['FLASK_ENV'] = 'development'

DEFAULT_HOST = "0.0.0.0"
DEFAULT_PORT = 8000


if len(sys.argv) == 2:
    DEFAULT_PORT = sys.argv[-1]


app.run(host=DEFAULT_HOST, port=DEFAULT_PORT)
