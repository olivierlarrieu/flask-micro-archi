from base.permissions import AllowAll, IsAuthenticated
from base.response import JsonResponse
from base.views import BaseView


class HelloView(BaseView):

    # permission_classes = [IsAuthenticated, ]

    def list(self, request, lastname=None, username=None):

        data = {
            "method": request.method,
            "username": username,
            "name": lastname or request.args.get('name')
        }
        return JsonResponse(data=data, status=200)
