import sys
from pathlib import Path

from flask import Flask

from base.router import Router
from views.hello import HelloView

sys.path.append(Path('.').absolute().as_posix())


# create the Flask application
app = Flask('hello')


# register the application routes
with app.app_context():
    router = Router()

    router.register(
        '/hello',
        methods=['GET', ],
        view=HelloView
    )

    router.register(
        '/hello/<username>',
        methods=['GET', ],
        view=HelloView
    )

    router.register(
        '/hello/<username>/<lastname>',
        methods=['GET', ],
        view=HelloView
    )
