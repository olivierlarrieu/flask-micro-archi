from flask import Response, abort, jsonify, request
from base.permissions import AllowAll


METHOD_MAPPING = {
    "GET": "list",
    "POST": "create",
    "PUT": "update",
    "PATCH": "partial_update",
    "DELETE": "delete"
}


class BaseView():
    """
    Simple view inspired from Django.
    The view is able to map the method corresponding to the
    http methods.
    You can add permissions classes to prevent your view.
    """
    permission_classes = [AllowAll, ]

    @classmethod
    def as_view(cls, *args, **kwargs):
        return cls().dispatch

    def check_permissions(self, request):
        has_perm = all([p().has_perm(request) for p in self.permission_classes])
        return has_perm

    def dispatch(self, *args, **kwargs):
        if not self.check_permissions(request):
            return abort(403)

        methode_name = METHOD_MAPPING.get(request.method)
        method = getattr(self, methode_name)
        return method(request, *args, **kwargs)
