from flask import jsonify


def JsonResponse(data={}, status=200):
    """
    response wrapper for json data.
    Allow to return a more readable and pythonic object than a tuple.
    """
    return jsonify(data), status
