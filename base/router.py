from flask import current_app


class Router():
    """
    Simple Router class to simply registering routes.
    Flask application is recupered from current_app.
    This mean than you need to use the app.app_context() context manager
    when creating routes.
    """
    def __init__(self):
        self.app = current_app
        self.routes = []

    def register(self, path, methods, view):
        view_func = view
        if hasattr(view, "as_view"):
            view_func = view.as_view()
        route = self.app.route(path, endpoint=path, methods=methods)(view_func)
        route = {
            "route": route,
            "path": path,
            "methods": methods,
            "func": view_func
        }
        self.routes.append(route)
