'''
Permissions classes to be used in BaseView.
'''


class AllowAll():
    def has_perm(self, request):
        return True


class IsAuthenticated():
    def has_perm(self, request):
        # TODO: perform authentication
        return False
